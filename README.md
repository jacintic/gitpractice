# Esto es un titulo  
## Esto es un subtitulo y hay mas niveles  
A nice program that says hello and more. The struggle continues.  

* list
* list
    * sublist
```
sadsdadsd
    asdasdas
        asdasda
  aasdasd
```
`<div>` then text  
```
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>The HTML5 Herald</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">

</head>

<body>
  <script src="js/scripts.js"></script>
</body>
</html>
```
<b>hello</b>  
`<b>hello</b>`  
**bold**  
*italic*  
***italic and bold***  
  
## How to get access to all remote branches  
```
git pull --all
```
  
## How to push all changes to new branch from current  
```
git checkout -b new-branch-name
git push -u origin new-branch-name
```
