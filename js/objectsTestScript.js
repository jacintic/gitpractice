
const myVars = {
	myOl : document.getElementById('todoList'),
	plusButton : document.getElementById('post'),
	bgForm : document.getElementById('postBg'),
	myTextarea : document.getElementById('myTextarea'),
	myDate : document.getElementById('myDate')
}
const renderList = () => {
	const runTHroughArr = todoArr.map(function(item) {
		// superElement ol
		const mainList = myVars.myOl;
		// main task elelment
		const itemLi = document.createElement('li');
		// add completed class in clase it is completed / in progress
		item.completed === "true" ? itemLi.classList.add('completed') : item.completed === "inProgress" ? itemLi.classList.add('progress'):0;
		// add preferential class in case it is true
		item.preferential ? itemLi.classList.add('preferential') : 0;
		// contains sub elemnt
		const itemUl = document.createElement('ul');
		// text element
		const text = document.createElement('li');
		const myText = item.content;
		const textCont = document.createTextNode(myText);
		// date element
		const date = document.createElement('li');
		const myDate = item.date;
		const dateCont = document.createTextNode(myDate);
		// interaction  element
		const interactionElements = document.createElement('li');
		// items inside completed
		const favourite = document.createElement('div');
		const status = document.createElement('div');
		const deleter = document.createElement('div');
		// adding class to the divs
		favourite.classList.add('favorite');
		status.classList.add('status');
		deleter.classList.add('delete');
		// adding title to elements
		favourite.title = "favorite";
		item.completed === "true" ? status.title = "completed" : item.completed === "inProgress" ? status.title = "in progress" : status.title = "status";
		deleter.title = "delete";
		// adding divs to interaction
		interactionElements.appendChild(deleter);
		interactionElements.appendChild(status);
		interactionElements.appendChild(favourite);
		// packaging
		// text
		text.appendChild(textCont);
		// date
		date.appendChild(dateCont);
		// items into ul
		itemUl.appendChild(text);
		itemUl.appendChild(date);
		itemUl.appendChild(interactionElements);
		// li elemnt of main list (task li)
		itemLi.appendChild(itemUl);
		// packing into super element (ol)
		mainList.appendChild(itemLi);	
	});
}
// checks local storage for stored list
if (localStorage.getItem("myList")) {
	const retrievedData = localStorage.getItem("myList");
	const list = JSON.parse(retrievedData);
	todoArr = list;
}
renderList();
// interaction handlers
// delete
const deleteElement = (event) => {
	// ol element
	const olElement = myVars.myOl;
	// specific li element
	const liElement = event.target.parentElement.parentElement.parentElement;
	// get number of children to delete
	const index = Array.prototype.indexOf.call(olElement.children, liElement);
	// remove children li from ol
	olElement.removeChild(liElement);
	// delete element from obj array
	todoArr = todoArr.slice(0, index).concat(todoArr.slice(index + 1, todoArr.length));
	saveLS();
}
// status
const toggleStatus = (event) => {
	// ol element
	const olElement = myVars.myOl;
	// li element
	const liElement = event.target.parentElement.parentElement.parentElement;
	if (liElement.classList.contains('completed')) {
		liElement.classList.remove('completed');
		liElement.classList.add('progress');
		event.target.setAttribute('title', 'in progress');
	} else if (liElement.classList.contains('progress')) {
		liElement.classList.remove('progress');
		event.target.setAttribute('title', 'status');
	} else {
		liElement.classList.add('completed');
		event.target.setAttribute('title', 'completed');
	}
	const index = Array.prototype.indexOf.call(olElement.children, liElement);
	// modify favourite form obj array
	todoArr[index].completed = liElement.classList.contains('completed') ? "true" : liElement.classList.contains('progress') ? "inProgress" : false;
	saveLS();
}
// favorite
const toggleFavorite = (event) => {
	// ol element
	const olElement = myVars.myOl;
	const liElement = event.target.parentElement.parentElement.parentElement;
	liElement.classList.toggle('preferential');
	// clone element
	const clonedLi = liElement.cloneNode(true);
	
	if (liElement.classList.contains('preferential')) {
		 // clone array element
		 const indexOrig = Array.prototype.indexOf.call(olElement.children, liElement);
		 const thisObject = todoArr[indexOrig];
		 // delete element
		 deleteElement(event);
		 // insert array element
		 todoArr.unshift(thisObject);
		 
		 let theFirstChild = olElement.firstChild;
		 // Create a new element

		 // Insert the new element before the first child
		 olElement.insertBefore(clonedLi, theFirstChild);
		 
		// update array and save to local storage
		// get index of element
		const index = Array.prototype.indexOf.call(olElement.children, clonedLi);
		 // modify favourite form obj array
		todoArr[index].preferential = liElement.classList.contains('preferential') ? true : false;
		saveLS();
	} else {
		// clone array element
		const indexOrig = Array.prototype.indexOf.call(olElement.children, liElement);
		const thisObject = todoArr[indexOrig];
		// delete element
		deleteElement(event);
		const afterFavIndex = document.getElementsByClassName('preferential').length;
		// insert array element
		todoArr.splice(afterFavIndex,0,thisObject);
		 const firstNonfav = olElement.childNodes[afterFavIndex + 1];
		 // Create a new element

		 // Insert the new element before the first child
		 olElement.insertBefore(clonedLi, firstNonfav);
		 
		// update array and save to local storage
		// get index of element
		
		const index = Array.prototype.indexOf.call(olElement.children, clonedLi);
		 // modify favourite form obj array
		todoArr[afterFavIndex].preferential = liElement.classList.contains('preferential') ? true : false;
		saveLS();
	}
	
}
// click handler (tasks manipulation)
document.getElementById("todoList").addEventListener('click', event => {
	const expr = event.target.className;
	switch (expr) {
	  case 'delete':
		deleteElement(event);
		break;
	  case 'status':
		toggleStatus(event);
		break;
	  case 'favorite':
		toggleFavorite(event);
	    break;
	break;
	  default:
		return;
}
});

/* save to localstorage */
const saveLS = () => {
	const stringifiedArr = JSON.stringify(todoArr);
	localStorage.setItem("myList", stringifiedArr);
}

/* post comment */
const postComment = () => {
	if (document.getElementById('myTextarea').value !== '') {
		// superElement ol
		const mainList = myVars.myOl;
		// status variables
		// variables is completed
		const isCompleted = document.getElementById('isStatus').classList.contains('completed');
		// variables is in progress
		const isProgress = document.getElementById('isStatus').classList.contains('progress');
		// favorite variables
		const isFavorite = document.getElementById('isFavourite').classList.contains('active');
		// main task elelment
		const itemLi = document.createElement('li');
		// add completed class in clase it is completed / in progress
		isCompleted ? itemLi.classList.add('completed') : isProgress ? itemLi.classList.add('progress'):0;
		// add preferential class in case it is true
		isFavorite ? itemLi.classList.add('preferential') : 0;
		// contains sub elemnt
		const itemUl = document.createElement('ul');
		// text element
		const text = document.createElement('li');
		const myText = document.getElementById('myTextarea').value;
		const textCont = document.createTextNode(myText);
		// date element
		const date = document.createElement('li');
		const myDate = document.getElementById('myDate').value
		const dateCont = document.createTextNode(myDate);
		// interaction  element
		const interactionElements = document.createElement('li');
		// items inside completed
		const favourite = document.createElement('div');
		const status = document.createElement('div');
		const deleter = document.createElement('div');
		// adding class to the divs
		favourite.classList.add('favorite');
		status.classList.add('status');
		deleter.classList.add('delete');
		// adding title to elements
		favourite.title = "favorite";
		isCompleted === "true" ? status.title = "completed" : isProgress ? status.title = "in progress" : status.title = "in progress";
		deleter.title = "delete";
		// adding divs to interaction
		interactionElements.appendChild(deleter);
		interactionElements.appendChild(status);
		interactionElements.appendChild(favourite);
		// packaging
		// text
		text.appendChild(textCont);
		// date
		date.appendChild(dateCont);
		// items into ul
		itemUl.appendChild(text);
		itemUl.appendChild(date);
		itemUl.appendChild(interactionElements);
		// li elemnt of main list (task li)
		itemLi.appendChild(itemUl);
		// packing into super element (ol)
		// conditional is favorite
		
		
		
		let theFirstChild;
		let afterFavIndex;
		if (isFavorite) {
		// special insertBefore
			theFirstChild = mainList.firstChild
		// Create a new element
		} else {
			if (afterFavIndex = document.getElementsByClassName('preferential').length >= 1){
				afterFavIndex = document.getElementsByClassName('preferential').length;
				theFirstChild = mainList.childNodes[afterFavIndex + 1];
			} else {
				theFirstChild = mainList.firstChild
			}
		}
		
		
		// Insert the new element before the first child
		mainList.insertBefore(itemLi, theFirstChild)
		//mainList.appendChild(itemLi);	
		
		// add item to the json
		const myNewObject = {};
		myNewObject.id = todoArr.lenght + 1;
		myNewObject.preferential = isFavorite;
		let myStatus;
		if (isCompleted) {
			myStatus  = "true";
		} else if (isProgress) {
			myStatus = "inProgress";
		} else {myStatus = false;}
		myNewObject.completed = myStatus;
		myNewObject.date = myDate;
		myNewObject.content = myText;
		// if favorite send to top, else send to after favorites
		if (isFavorite) {
				todoArr.unshift(myNewObject);
		} else {
			const lasFavIndex = document.getElementsByClassName('preferential').length;
			todoArr.splice(lasFavIndex,0,myNewObject);
		}
	
		
		
		// hide form
		// plus element
		const plusElem = myVars.plusButton;
		const formElem = myVars.bgForm;
		plusElem.classList.toggle('isPosting');
		formElem.classList.toggle('active');
		
		// clear form
		const textarElem = document.getElementById('myTextarea');
		textarElem.value = '';
		controlTa();
		// date
		const dateElem = document.getElementById('myDate');
		dateElem.value = '';
		controlDate();
		// status elements
		// favorite
		if (document.getElementById('isFavourite').classList.contains('active')) {
			document.getElementById('isFavourite').classList.toggle('active');
		}
		// status
		const postStatus = document.getElementById('isStatus');
		if (postStatus.classList.contains('completed')) {
			postStatus.classList.toggle('completed')
		} else if (postStatus.classList.contains('progress')) {
			postStatus.classList.toggle('progress')
		}
		
		// post comment (whole list) to localStorage
		saveLS();
	}
}

// click handler (+ post button)

document.getElementById('post').addEventListener('click', event => {
	// plus element
	const plusElem = myVars.plusButton;
	const formElem = myVars.bgForm;
	plusElem.classList.toggle('isPosting');
	formElem.classList.toggle('active');
	document.getElementById('myTextarea').focus();
});


// control text area funcion
const controlTa = () => {
	const thisLabel = document.getElementById('taLb');
    const textarElem = document.getElementById('myTextarea');
	if (textarElem.value !== '') {
		thisLabel.classList.add('hidden');
	} else {
		thisLabel.classList.remove('hidden');
	}
}

// control date

const controlDate = () => {
	const thisLabel = document.getElementById('itLb');
    const textarElem = document.getElementById('myDate');
	if (textarElem.value !== '') {
		thisLabel.classList.add('hidden');
	} else {
		thisLabel.classList.remove('hidden');
	}
}

// textarea placeholder managing
document.getElementById('myTextarea').addEventListener('input', () => {
	controlTa();
});

document.getElementById('myDate').addEventListener('input', () => {
	controlDate();
});

// click handler (favourites post button)

document.getElementById('isFavourite').addEventListener('click', event => {
	document.getElementById('isFavourite').classList.toggle('active');
});

// click handler (status post button)

document.getElementById('isStatus').addEventListener('click', event => {
	const postStatus = document.getElementById('isStatus');
	if (postStatus.classList.contains('completed')) {
		postStatus.classList.remove('completed');
		postStatus.classList.add('progress');
		postStatus.setAttribute('title', 'in progress');
	} else if (postStatus.classList.contains('progress')) {
		postStatus.classList.remove('progress');
		postStatus.setAttribute('title', 'status');
	} else {
		postStatus.classList.add('completed');
		postStatus.setAttribute('title', 'completed');
	}
});

// click handler (post form)
document.getElementById('isPost').addEventListener('click', event => {
	postComment();
});
